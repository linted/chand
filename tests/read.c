/*
 * Copyright 2014 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _GNU_SOURCE

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define HELLO "hello world"

int main(void)
{
	int fd = open("dir/foo", O_CREAT | O_NONBLOCK | O_CLOEXEC | O_RDWR, S_IRWXU);
	if (-1 == fd) {
		perror("open");
		return EXIT_FAILURE;
	}

	{
		char foo[sizeof HELLO] = {0};
		ssize_t bytes = read(fd, foo, sizeof foo);
		if (-1 == bytes) {
			perror("read");
			return EXIT_FAILURE;
		}

		fprintf(stderr, "read: %lu %s\n", bytes, foo);
	}

	return EXIT_SUCCESS;
}
