/*
 * Copyright 2014 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE
#define FUSE_USE_VERSION 30

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define ARRAY_SIZE(X) (sizeof X / sizeof X[0U])

struct file_desc;
struct filesystem;
struct file_desc;
struct inode;
struct poller;

static int file_desc_open(struct file_desc **descp,
			  struct inode *dirinode, char const *path,
			  mode_t mode, unsigned flags);
static void file_desc_release(struct file_desc *desc);

static void file_desc_set_poller(struct file_desc *desc,
				 struct poller *poller);
static unsigned long file_desc_flags(struct file_desc *desc);
static void file_desc_set_flags(struct file_desc *desc,
				unsigned long flags);
static struct inode *file_desc_peek_inode(struct file_desc *desc,
					  int perm);


#define INODES_MAX 2048U
#define CHAND_PATH_MAX 255U

struct chanstatfs {
	fsfilcnt_t inodes;
};

struct chanstat {
	struct timespec last_access_time;
	struct timespec last_modification_time;
	struct timespec last_status_change;

	mode_t mode;

	uid_t owner;
	gid_t group;

	nlink_t links;
};

struct filesystem;
struct file_desc;
struct inode;
struct poller;

enum inode_type {
	INODE_TYPE_DIR,
	INODE_TYPE_CHAN
};

enum {
	INODE_TOUCH_ACCESS = 1,
	INODE_TOUCH_MODIFY = (1 << 1U),
	INODE_TOUCH_STATUS = (1 << 2U),
	INODE_TOUCH_ALL = INODE_TOUCH_ACCESS | INODE_TOUCH_MODIFY | INODE_TOUCH_STATUS,
};

static int filesystem_init(struct filesystem **fsp);
static void filesystem_destroy(struct filesystem *filesystem);

static struct inode *filesystem_root_peek(struct filesystem *fs);

static struct chanstatfs *filesystem_stat_get_lock(struct filesystem *fs);
static void filesystem_stat_put_lock(struct filesystem *fs);

static ino_t inode_number(struct inode *inode);
static enum inode_type inode_type(struct inode *inode);

static struct inode *inode_get(struct inode *inode);
static void inode_put(struct inode *inode);

static struct chanstat *inode_stat_get_lock(struct inode *inode);
static void inode_stat_put_lock(struct inode *inode);

static int inode_access(struct inode *inode, int req);

static int inode_readdir(struct inode *inode, void *buf, fuse_fill_dir_t filler);

static int inode_poll(struct inode *inode,
		      struct fuse_pollhandle *ph, unsigned *reventsp,
		      struct poller **pollerp);
static int inode_destroy_poller(struct inode *inode, struct poller *poller);

static int inode_read(struct inode *inode, struct fuse_bufvec **bufp,
		      size_t size,
		       bool nonblocking);
static int inode_write(struct inode *inode, struct fuse_bufvec *buf,
		       bool nonblockingly);

static int inode_open(struct inode **inodep,
		      struct inode *dirinode, char const *path,
		      mode_t mode, unsigned flags);

static int inode_unlink(struct inode *inode, char const *path);

static int inode_rename(struct inode *inode, char const * oldpath,
			char const *newpath);
static int inode_chmod(struct inode *inode, mode_t mode);
static int inode_chown(struct inode *inode, uid_t owner,
		       gid_t group);

static int inode_truncate(struct inode *inode, off_t size);
static int inode_utimens(struct inode *inode, struct timespec const times[2U]);


static struct file_desc *peek_file_info_desc(struct fuse_file_info *fi);
static void set_file_info_desc(struct fuse_file_info *info,
			       struct file_desc *desc);

static void secure_zero(void volatile *buf, size_t size);


static int chand_getattr(char const * path, struct stat * stat_buffer);
static int chand_fgetattr(char const * path, struct stat * stat_buffer,
			  struct fuse_file_info *file_info);

static int chand_truncate(const char *path, off_t size);

static int chand_utimens(const char *path, struct timespec const tv[2]);

static int chand_rename(char const * oldpath, char const * newpath);

static int chand_chmod(char const * path, mode_t mode);
static int chand_chown(char const * path, uid_t owner, gid_t group);

static int chand_create(char const *path, mode_t mode,
			struct fuse_file_info *file_info);
static int chand_access(char const * path, int mode);
static int chand_open(char const * path, struct fuse_file_info * file_info);
static int chand_opendir(char const * path, struct fuse_file_info * file_info);

static int chand_poll(const char *path, struct fuse_file_info *file_info,
		      struct fuse_pollhandle *ph, unsigned *reventsp);

static int chand_read_buf(char const * path, struct fuse_bufvec **bufp,
			  size_t size, off_t offset,
			  struct fuse_file_info * file_info);
static int chand_write_buf(char const * path, struct fuse_bufvec *buf,
			   off_t offset, struct fuse_file_info * file_info);

static int chand_statfs(char const * path, struct statvfs * buf);

static int chand_readdir(char const * path,
			 void * buf,
			 fuse_fill_dir_t filler,
			 off_t offset,
			 struct fuse_file_info * file_info);

static int chand_release(char const * path,
			 struct fuse_file_info * file_info);
static int chand_releasedir(const char *path,
			    struct fuse_file_info *file_info);
static int chand_unlink(char const * path);

static void * chand_init(struct fuse_conn_info *conn);
static void chand_destroy(void * userdata);

static int chand_readlink(const char *path, char *buf, size_t size);
static int chand_mknod(const char *path, mode_t mode, dev_t dev);
static int chand_mkdir(const char *path, mode_t mode);
static int chand_rmdir(const char *path);
static int chand_symlink(const char *path, char const *dest);
static int chand_link(const char *path, char const *dest);
static int chand_flush(const char *path, struct fuse_file_info *file_info);
static int chand_fsync(const char *path, int datasync,
		       struct fuse_file_info *file_info);
static int chand_getxattr(const char *path, char const *name, char *buf,
			  size_t size);
static int chand_setxattr(const char *path, const char * name, const char *value,
			  size_t size, int flags);
static int chand_listxattr(const char *path, char *list, size_t size);
static int chand_removexattr(const char *path, const char * name);
static int chand_fsyncdir(const char *path, int datasync,
			  struct fuse_file_info *file_info);
static int chand_ftruncate(const char *path, off_t size,
			   struct fuse_file_info *file_info);
static int chand_ioctl(const char *path, int cmd, void *arg,
		       struct fuse_file_info *info, unsigned flags, void*data);
static int chand_fallocate(const char *path, int mode, off_t size,
			   off_t len,
			   struct fuse_file_info *file_info);

static struct fuse_operations const chand_operations = {
	.getattr = chand_getattr,

	.readlink = chand_readlink,

	.mknod = chand_mknod,

	.mkdir = chand_mkdir,

	.unlink = chand_unlink,

	.rmdir = chand_rmdir,

	.symlink = chand_symlink,

	.rename = chand_rename,

	.link = chand_link,

	.chmod = chand_chmod,

	.chown = chand_chown,

	.truncate = chand_truncate,

	.open = chand_open,

	.statfs = chand_statfs,

	.flush = chand_flush,

	.release = chand_release,

	.fsync = chand_fsync,

	.setxattr = chand_setxattr,
	.getxattr = chand_getxattr,
	.listxattr = chand_listxattr,
	.removexattr = chand_removexattr,

	.opendir = chand_opendir,

	.readdir = chand_readdir,

	.releasedir = chand_releasedir,

	.fsyncdir = chand_fsyncdir,

	.init = chand_init,
	.destroy = chand_destroy,

	.access = chand_access,

	.create = chand_create,

	.ftruncate = chand_ftruncate,

	.fgetattr = chand_fgetattr,

	/*
	 * Note: if this method is not implemented, the kernel will still
	 * allow file locking to work locally.  Hence it is only
	 * interesting for network filesystems and similar.
	 *
	 * Introduced in version 2.6
	 */
	/* .lock = chand_lock, */

	.utimens = chand_utimens,

	/* Only makes sense for device based filesystems */
	/* .bmap = chand_bmap, */

	.flag_nullpath_ok = true,
	.flag_nopath = true,

	.flag_utime_omit_ok = true,

	.ioctl = chand_ioctl,

	.poll = chand_poll,

	.write_buf = chand_write_buf,
	.read_buf = chand_read_buf,

	/**
	 * Note: if this method is not implemented, the kernel will still
	 * allow file locking to work locally.  Hence it is only
	 * interesting for network filesystems and similar.
	 */
	/* .flock = chand_flock, */

	.fallocate = chand_fallocate
};

int main(int argc, char *argv[])
{
	return fuse_main(argc, argv, &chand_operations, NULL);
}

static int chand_getattr(char const * path, struct stat * stat_buffer)
{
	int errnum = 0;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, 0U);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	enum inode_type type;

	ino_t inode_num;
	nlink_t nlink;
	mode_t mode;

	uid_t owner;
	gid_t group;

	struct timespec last_access_time;
	struct timespec last_modification_time;
	struct timespec last_status_change;

	{
		struct inode *inode = file_desc_peek_inode(desc, 0);
		assert(inode != NULL);

		inode_num = inode_number(inode);
		type = inode_type(inode);

		struct chanstat *stat = inode_stat_get_lock(inode);

		nlink = stat->links;
		mode = stat->mode;

		owner = stat->owner;
		group = stat->group;

		last_access_time = stat->last_access_time;
		last_modification_time = stat->last_modification_time;
		last_status_change = stat->last_status_change;

		inode_stat_put_lock(inode);
	}

	file_desc_release(desc);

	switch (type) {
	case INODE_TYPE_DIR:
		mode |= S_IFDIR;
		break;

	case INODE_TYPE_CHAN:
		mode |= S_IFREG;
		break;

	default:
		assert(false);
	}

	secure_zero(stat_buffer, sizeof *stat_buffer);

	stat_buffer->st_dev = 0; /* ignored */

	stat_buffer->st_ino = inode_num;
	stat_buffer->st_nlink = nlink;
	stat_buffer->st_mode = mode;

	stat_buffer->st_uid = owner;
	stat_buffer->st_gid = group;

	stat_buffer->st_rdev = 0; /* ignored */

	/* Zero is the convention for magic files */
	stat_buffer->st_size = 0U;

	stat_buffer->st_blksize = 0U; /* ignored */
	stat_buffer->st_blocks = 0U;  /* ignored */

	stat_buffer->st_atim = last_access_time;
	stat_buffer->st_mtim = last_modification_time;
	stat_buffer->st_ctim = last_status_change;

	return 0;
}

static int chand_fgetattr(char const * path, struct stat * stat_buffer,
			  struct fuse_file_info *fi)
{
	struct file_desc *desc = peek_file_info_desc(fi);

	enum inode_type type;

	ino_t inode_num;
	nlink_t nlink;
	mode_t mode;

	uid_t owner;
	gid_t group;

	struct timespec last_access_time;
	struct timespec last_modification_time;
	struct timespec last_status_change;

	{
		struct inode *inode = file_desc_peek_inode(desc, 0);
		assert(inode != NULL);

		inode_num = inode_number(inode);
		type = inode_type(inode);

		struct chanstat *stat = inode_stat_get_lock(inode);

		nlink = stat->links;
		mode = stat->mode;

		owner = stat->owner;
		group = stat->group;

		last_access_time = stat->last_access_time;
		last_modification_time = stat->last_modification_time;
		last_status_change = stat->last_status_change;

		inode_stat_put_lock(inode);
	}

	switch (type) {
	case INODE_TYPE_DIR:
		mode |= S_IFDIR;
		break;

	case INODE_TYPE_CHAN:
		mode |= S_IFREG;
		break;

	default:
		assert(false);
	}

	secure_zero(stat_buffer, sizeof *stat_buffer);

	stat_buffer->st_dev = 0; /* ignored */

	stat_buffer->st_ino = inode_num;
	stat_buffer->st_nlink = nlink;
	stat_buffer->st_mode = mode;

	stat_buffer->st_uid = owner;
	stat_buffer->st_gid = group;

	stat_buffer->st_rdev = 0; /* ignored */

	/* Zero is the convention for magic files */
	stat_buffer->st_size = 0U;

	stat_buffer->st_blksize = 0U; /* ignored */
	stat_buffer->st_blocks = 0U;  /* ignored */

	stat_buffer->st_atim = last_access_time;
	stat_buffer->st_mtim = last_modification_time;
	stat_buffer->st_ctim = last_status_change;

	return 0;
}

static int chand_truncate(const char *path, off_t size)
{
	int errnum = 0;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, O_WRONLY);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	{
		struct inode *inode = file_desc_peek_inode(desc, W_OK);

		errnum = inode_truncate(inode, size);
	}

	file_desc_release(desc);

	return errnum;
}

static int chand_utimens(const char *path, struct timespec const tv[2])
{
	int errnum = 0;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	/* TODO: Allow the owner of the file to change the last access
	 * times even if he doesn't have write permission.
	 */

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, O_WRONLY);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	{
		struct inode *inode = file_desc_peek_inode(desc, W_OK);
		if (NULL == inode) {
			errnum = -EPERM;
			goto release_desc;
		}

		errnum = inode_utimens(inode, tv);
	}

release_desc:
	file_desc_release(desc);

	return errnum;
}

static int chand_rename(char const * oldpath, char const * newpath)
{
	struct filesystem *data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	return inode_rename(root, oldpath + 1U, newpath + 1U);
}

static int chand_chmod(char const * path, mode_t mode)
{
	int errnum = 0;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, 0);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}
	{
		struct inode *inode = file_desc_peek_inode(desc, 0);

		errnum = inode_chmod(inode, mode);
	}

	file_desc_release(desc);

	return errnum;
}

static int chand_chown(char const * path, uid_t owner, gid_t group)
{
	int errnum = 0;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, 0);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}
	{
		struct inode *inode = file_desc_peek_inode(desc, 0);

		errnum = inode_chown(inode, owner, group);
	}

	file_desc_release(desc);

	return errnum;
}

static int chand_create(char const *path, mode_t mode,
			struct fuse_file_info *file_info)
{
	int errnum = 0;

	unsigned flags = file_info->flags;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode * root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, mode, flags);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	set_file_info_desc(file_info, desc);

	return errnum;
}

static int chand_open(char const *path, struct fuse_file_info *file_info)
{

	int errnum = 0;

	unsigned flags = file_info->flags;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1,
					flags & ~(unsigned)O_CREAT & ~(unsigned)O_EXCL);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	set_file_info_desc(file_info, desc);

	return errnum;
}

static int chand_opendir(char const *path, struct fuse_file_info *file_info)
{
	int errnum = 0;

	unsigned flags = file_info->flags;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	struct file_desc *file;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, flags | O_DIRECTORY);
		if (errnum < 0)
			return errnum;
		file = xx;
	}

	set_file_info_desc(file_info, file);

	return errnum;
}

static int chand_poll(const char *path, struct fuse_file_info *file_info,
		      struct fuse_pollhandle *ph, unsigned *reventsp)
{
	struct file_desc *desc = peek_file_info_desc(file_info);

	struct inode *inode = file_desc_peek_inode(desc, 0);
	if (NULL == inode) {
		if (ph != NULL)
			fuse_pollhandle_destroy(ph);
		return -EPERM;
	}

	struct poller *poller;
	int errnum = inode_poll(inode, ph, reventsp, &poller);
	if (errnum < 0)
		return errnum;

	file_desc_set_poller(desc, poller);

	return errnum;
}

static int chand_read_buf(char const * path, struct fuse_bufvec **bufp,
			  size_t size, off_t offset,
			  struct fuse_file_info * file_info)
{
	struct file_desc *desc = peek_file_info_desc(file_info);

	struct inode *inode = file_desc_peek_inode(desc, R_OK);
	if (NULL == inode)
		return -EPERM;

	unsigned long flags = file_desc_flags(desc);

	return inode_read(inode, bufp, size, (flags & O_NONBLOCK) != 0);
}

static int chand_write_buf(char const * path, struct fuse_bufvec *buf,
			   off_t offset, struct fuse_file_info * file_info)
{
	struct file_desc *desc = peek_file_info_desc(file_info);

	struct inode *inode = file_desc_peek_inode(desc, W_OK);
	if (NULL == inode)
		return -EPERM;

	unsigned long flags = file_desc_flags(desc);

	return inode_write(inode, buf, (flags & O_NONBLOCK) != 0);
}

static int chand_statfs(char const * path, struct statvfs *buf)
{
	struct filesystem *fs = fuse_get_context()->private_data;

	unsigned long inodes;
	{
		struct chanstatfs *stat = filesystem_stat_get_lock(fs);

		inodes = stat->inodes;

		filesystem_stat_put_lock(fs);
	}

	secure_zero(buf, sizeof *buf);

	/* Must be 1 so that reads and writes aren't broken up into
	 * larger chunks.*/
	buf->f_bsize = 1U;
	buf->f_frsize = 0U;	/* ignored */
	buf->f_blocks = 0U;
	buf->f_bfree = 0U;
	buf->f_bavail = 0U;

	buf->f_files = INODES_MAX;
	buf->f_ffree = inodes;

	buf->f_favail = 0U;	/* ignored */

	buf->f_fsid = 0U;	/* ignored */
	buf->f_flag = 0U; /* ignored */
	buf->f_namemax = CHAND_PATH_MAX;

	return 0;
}

static int chand_readdir(char const *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi)
{
	struct file_desc *desc = peek_file_info_desc(fi);

	struct inode *inode = file_desc_peek_inode(desc, 0);
	if (NULL == inode)
		return -EPERM;

	return inode_readdir(inode, buf, filler);
}

static int chand_access(char const * path, int req)
{
	int errnum;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	if ((req & ~(F_OK | R_OK | W_OK | X_OK)) != 0)
		return -EINVAL;

	struct file_desc *desc;
	{
		struct file_desc *xx;
		errnum = file_desc_open(&xx, root, path + 1U, -1, 0U);
		if (errnum < 0)
			return errnum;
		desc = xx;
	}

	{
		struct inode *inode = file_desc_peek_inode(desc, 0);

		errnum = inode_access(inode, req);
	}

	file_desc_release(desc);

	return errnum;
}

static int chand_release(char const * path,
			 struct fuse_file_info * fi)
{
	struct file_desc *desc = peek_file_info_desc(fi);

	file_desc_release(desc);

	return 0;
}

static int chand_releasedir(const char *path,
			    struct fuse_file_info *file_info)
{
	struct file_desc *desc = peek_file_info_desc(file_info);

	file_desc_release(desc);

	return 0;
}

static int chand_unlink(char const * path)
{
	if (0 == strcmp("/", path))
		return -EBUSY;

	struct filesystem * data = fuse_get_context()->private_data;
	struct inode *root = filesystem_root_peek(data);

	return inode_unlink(root, path + 1U);
}

static void * chand_init(struct fuse_conn_info *conn)
{
	int errnum;

	struct filesystem * fs;
	errnum = filesystem_init(&fs);
	if (errnum < 0) {
		errno = -errnum;
		perror("filesystem_init");
		exit(EXIT_FAILURE);
	}

	return fs;
}

static void chand_destroy(void * userdata)
{
	struct filesystem * fs = userdata;

	filesystem_destroy(fs);
}

static int chand_readlink(const char *path, char *buf, size_t size)
{
	return -EINVAL;
}

static int chand_mknod(const char *path, mode_t mode, dev_t dev)
{
	return -EPERM;
}

static int chand_mkdir(const char *path, mode_t mode)
{
	return -EPERM;
}

static int chand_rmdir(const char *path)
{
	return -EPERM;
}

static int chand_symlink(const char *path, char const *dest)
{
	return -EPERM;
}

static int chand_link(const char *path, char const *dest)
{
	return -EPERM;
}

static int chand_flush(const char *path, struct fuse_file_info *file_info)
{
	return 0;
}

static int chand_fsync(const char *path, int datasync,
		       struct fuse_file_info *file_info)
{
	return 0;
}

static int chand_getxattr(const char *path, char const *name, char *buf,
			  size_t size)
{
	return -ENOTSUP;
}

static int chand_setxattr(const char *path, const char * name,
			  const char *value,
			  size_t size, int flags)
{
	return -ENOTSUP;
}

static int chand_listxattr(const char *path, char *list, size_t size)
{
	return -ENOTSUP;
}

static int chand_removexattr(const char *path, const char * name)
{
	return -ENOTSUP;
}

static int chand_fsyncdir(const char *path, int datasync,
			  struct fuse_file_info *file_info)
{
	return 0;
}

static int chand_ftruncate(const char *path, off_t size,
			   struct fuse_file_info *file_info)
{
	return -EPERM;
}

static int chand_ioctl(const char *path, int cmd, void *arg,
		       struct fuse_file_info *info, unsigned flags, void*data)
{
	return -EINVAL;
}

static int chand_fallocate(const char *path, int mode, off_t size,
			   off_t len,
			   struct fuse_file_info *file_info)
{
	return -EPERM;
}

struct file_desc {
	struct inode *inode;

	pthread_spinlock_t lock;
	struct poller *poller;
	unsigned long flags;

	int perm;
};

static int file_desc_open(struct file_desc **descp,
			  struct inode *dirinode, char const *path,
			  mode_t mode, unsigned flags)
{
	int errnum = 0;

	unsigned mode_open_flags = O_RDONLY | O_WRONLY | O_RDWR;

	unsigned misc_flags = O_APPEND | O_NONBLOCK | O_TRUNC | O_DIRECTORY;

	/* Strangely, the sign bit is set in the flags passed to use
	 * from FUSE */
	unsigned sign_bit = 0x8000U;

	unsigned creation_flags = O_CREAT | O_EXCL;

	unsigned valid_flags = mode_open_flags | creation_flags | misc_flags
		| sign_bit;

	if ((flags & ~valid_flags) != 0U)
		return -EINVAL;

	bool o_creat = (flags & O_CREAT) != 0;
	bool o_excl = (flags & O_EXCL) != 0;

	bool o_rdonly = (flags & O_RDONLY) != 0;
	bool o_wronly = (flags & O_WRONLY) != 0;
	bool o_rdwr = (flags & O_RDWR) != 0;

	bool o_append = (flags & O_APPEND) != 0;
	bool o_nonblock = (flags & O_NONBLOCK) != 0;

	bool o_directory = (flags & O_DIRECTORY) != 0;

	if (o_excl && !o_creat)
		return -EINVAL;

	if (o_directory && o_creat)
		return -EINVAL;

	/* Some applications open directories with the following even
	 * though they don't make sense for directories. Be
	 * compatible.
	 */
	if (o_directory && o_append)
		return -EINVAL;

	/* Allow O_DIRECTORY and O_NONBLOCK */

	/* Applications shall specify exactly one of the first five
	 * values (file access modes) below in the value of oflag: -
	 * POSIX 2008
	 */

	unsigned values = 0U;
	if (o_rdonly) ++values;
	if (o_wronly) ++values;
	if (o_rdwr) ++values;

	/* According to a strict interpretation of POSIX a value of 0
	 * should be disallowed but we need to be compatible with
	 * existing applications. */
	if (values > 1U)
		return -EINVAL;

	int description_flags = 0;
	if (o_append)
		description_flags |= O_APPEND;

	if (o_nonblock)
		description_flags |= O_NONBLOCK;

	int perms = 0;
	if (o_rdonly || o_rdwr)
		perms |= R_OK;
	if (o_wronly || o_rdwr)
		perms |= W_OK;

	struct file_desc *desc = malloc(sizeof *desc);
	if (NULL == desc)
		return -errno;

	inode_get(dirinode);
	for (;;) {
		char *end = strchr(path, '/');
		if (NULL == end)
			break;

		errnum = inode_access(dirinode, X_OK);
		if (errnum < 0)
			goto put_dirinode;

		size_t len = end - path;

		char *path_chunk = strndup(path, len);
		if (NULL == path_chunk) {
			errnum = -errno;
			goto free_desc;
		}

		struct inode *new_dir;
		{
			struct inode *xx;
			errnum = inode_open(&xx, dirinode,
					    path_chunk, -1,
					    O_DIRECTORY);
			if (errnum < 0)
				goto free_path_chunk;
			new_dir = xx;
		}
		inode_put(dirinode);
		dirinode = new_dir;

		path += len + 1U;

	free_path_chunk:
		free(path_chunk);
		if (errnum < 0)
			goto free_desc;
	}

	struct inode *inode;
	{
		struct inode *xx;
		errnum = inode_open(&xx, dirinode, path, mode, flags);
		if (errnum < 0)
			goto put_dirinode;
		inode = xx;
	}

	desc->inode = inode;

	pthread_spin_init(&desc->lock, false);
	desc->flags = description_flags;
	desc->poller = NULL;

	desc->perm = perms;

	*descp = desc;

put_dirinode:
	inode_put(dirinode);

free_desc:
	if (errnum < 0)
		free(desc);

	return errnum;
}

static void file_desc_set_poller(struct file_desc *desc,
				 struct poller *poller)
{
	pthread_spin_lock(&desc->lock);

	if (desc->poller != NULL)
		inode_destroy_poller(desc->inode, desc->poller);

	desc->poller = poller;

	pthread_spin_unlock(&desc->lock);
}

static unsigned long file_desc_flags(struct file_desc *desc)
{
	unsigned long flags;

	pthread_spin_lock(&desc->lock);
	flags = desc->flags;
	pthread_spin_unlock(&desc->lock);

	return flags;
}

static void file_desc_set_flags(struct file_desc *desc,
				unsigned long flags)
{
	pthread_spin_lock(&desc->lock);
	desc->flags = flags;
	pthread_spin_unlock(&desc->lock);
}

static struct inode *file_desc_peek_inode(struct file_desc *desc,
					  int req)
{
	int perm = desc->perm;

	bool r_ok = (perm & R_OK) != 0;
	bool w_ok = (perm & W_OK) != 0;

	bool req_r_ok = (req & R_OK) != 0;
	bool req_w_ok = (req & W_OK) != 0;

	if (req_r_ok && !r_ok)
		return NULL;

	if (req_w_ok && !w_ok)
		return NULL;

	return desc->inode;
}

static void file_desc_release(struct file_desc *desc)
{
	struct inode *inode = file_desc_peek_inode(desc, 0);

	if (desc->poller != NULL)
		inode_destroy_poller(inode, desc->poller);

	inode_put(inode);

	free(desc);
}

struct inode_common
{
	bool active: 1U;
};

struct free_inode
{
	struct inode_common common;
	struct inode *next;
};

struct active_common
{
	struct inode_common common;
	struct filesystem *fs;
	enum inode_type type;

	struct chanstat stat;
	pthread_spinlock_t stat_lock;

	unsigned long refcount;
	pthread_spinlock_t refcount_lock;
};

struct direntry {
	char *name;
	struct inode *inode;
};

struct dir {
	struct active_common common;

	size_t salt;

	pthread_spinlock_t lock;
	size_t entries_size;
	struct direntry *entries;
};

struct poller {
	struct fuse_pollhandle *poll_handle;
	struct poller *next;
};

struct chan {
	struct active_common common;

	pthread_mutex_t lock;
	pthread_cond_t cond;

	struct fuse_bufvec **pending_reader;
	size_t pending_read_size;

	struct poller *first_poller;
};

union active_inode {
	struct active_common common;
	struct dir dir;
	struct chan chan;
};

union inode_union {
	struct inode_common common;
	struct free_inode free;
	union active_inode active;
};

struct inode {
	union inode_union impl;
};

struct filesystem {
	struct inode *root;
	int rand_fd;

	pthread_spinlock_t lock;

	struct chanstatfs stat;

	struct inode *free_inode;
	struct inode inode_table[INODES_MAX];
};

static int filesystem_inode_create(struct filesystem*fs, struct inode **inodep,
				   enum inode_type type, mode_t mode, uid_t uid,
				   gid_t gid);
static void inode_destroy(struct inode *inode);

static int inode_touch(struct inode *inode, int fields);

static unsigned long *inode_refcount_get_lock(struct inode *inode);
static void inode_refcount_put_lock(struct inode *inode);

static struct dir *inode_dir_peek(struct inode *inode);
static struct chan *inode_chan_peek(struct inode *inode);

static int filesystem_init(struct filesystem **fsp)
{
	int errnum;

	struct filesystem *fs = malloc(sizeof *fs);
	if (NULL == fs)
		return -errno;

	int rand_fd;
	do {
		rand_fd = open("/dev/random", O_RDONLY | O_CLOEXEC);
		if (-1 == rand_fd)
			errnum = -errno;
		else
			errnum = 0;
	} while (-EINTR == errnum);
	if (errnum < 0)
		return errnum;

	fs->rand_fd = rand_fd;

	pthread_spin_init(&fs->lock, false);

	struct inode *next = NULL;
	for (size_t ii = 0U; ii < ARRAY_SIZE(fs->inode_table); ++ii) {
		struct inode * inode = &fs->inode_table[ii];
		inode->impl.common.active = false;
		inode->impl.free.next = next;
		next = &fs->inode_table[ii];
	}
	fs->free_inode = next;
	fs->stat.inodes = ARRAY_SIZE(fs->inode_table);

	errnum = filesystem_inode_create(fs, &fs->root, INODE_TYPE_DIR, S_IRWXU,
					 getuid(), getgid());
	if (errnum < 0) {
		close(rand_fd);
		free(fs);
		return errnum;
	}

	*fsp = fs;
	return 0;
}

static void filesystem_destroy(struct filesystem *fs)
{
	struct inode *root = fs->root;

	inode_put(root);

	close(fs->rand_fd);
	free(fs);
}

static struct chanstatfs *filesystem_stat_get_lock(struct filesystem *fs)
{
	pthread_spin_lock(&fs->lock);
	return &fs->stat;
}

static void filesystem_stat_put_lock(struct filesystem *fs)
{
	pthread_spin_unlock(&fs->lock);
}

static struct inode *filesystem_root_peek(struct filesystem *fs)
{
	return fs->root;
}

static int filesystem_inode_create(struct filesystem*fs, struct inode **inodep,
				   enum inode_type type, mode_t mode, uid_t uid,
				   gid_t gid)
{
	int errnum = 0;

	struct inode *inode;

	pthread_spin_lock(&fs->lock);
	inode = fs->free_inode;
	if (inode != NULL) {
		fs->free_inode = inode->impl.free.next;
		--fs->stat.inodes;
	}
	pthread_spin_unlock(&fs->lock);

	if (NULL == inode)
		return -ENFILE;

	inode->impl.common.active = true;

	inode->impl.active.common.type = type;
	inode->impl.active.common.fs = fs;

	pthread_spin_init(&inode->impl.active.common.refcount_lock, false);
	inode->impl.active.common.refcount = 1U;

	pthread_spin_init(&inode->impl.active.common.stat_lock, false);

	inode->impl.active.common.stat.links = 0U;

	inode->impl.active.common.stat.mode = mode;

	inode->impl.active.common.stat.owner = uid;
	inode->impl.active.common.stat.group = gid;

	inode->impl.active.common.stat.last_access_time.tv_sec = 0;
	inode->impl.active.common.stat.last_access_time.tv_nsec = 0;

	inode->impl.active.common.stat.last_modification_time.tv_sec = 0;
	inode->impl.active.common.stat.last_modification_time.tv_nsec = 0;

	inode->impl.active.common.stat.last_status_change.tv_sec = 0;
	inode->impl.active.common.stat.last_status_change.tv_nsec = 0;

	switch (type) {
	case INODE_TYPE_DIR: {
		struct dir *dir = &inode->impl.active.dir;

		size_t bytes_left = sizeof dir->salt;
		do {
			ssize_t bytes_read = read(fs->rand_fd, &dir->salt, bytes_left);
			if (bytes_read < 0) {
				errnum = -errno;
				if (-EINTR == errnum) {
					errnum = 0;
					continue;
				}
				goto free_inode;
			}
			if (0 == bytes_read) {
				errnum = -EAGAIN;
				goto free_inode;
			}

			bytes_left -= bytes_read;
		} while (bytes_left != 0);

		pthread_spin_init(&dir->lock, false);
		dir->entries_size = 0U;
		dir->entries = NULL;
		break;
	}

	case INODE_TYPE_CHAN:
		pthread_mutex_init(&inode->impl.active.chan.lock, NULL);
		pthread_cond_init(&inode->impl.active.chan.cond, NULL);
		inode->impl.active.chan.pending_reader = NULL;
		inode->impl.active.chan.first_poller = NULL;
		break;

	default:
		assert(false);
	}

	/* Last thing we do is touch the file */
	errnum = inode_touch(inode, INODE_TOUCH_ALL);
	if (errnum < 0)
		goto free_inode;

	*inodep = inode;

free_inode:
	if (errnum < 0)
		inode_put(inode);

	return errnum;
}

static void inode_destroy(struct inode *inode)
{
	switch (inode_type(inode)) {
	case INODE_TYPE_CHAN: {
		struct chan *chan = inode_chan_peek(inode);
		break;
	}

	case INODE_TYPE_DIR: {
		/* TODO: Solve the recursive stack overflow problem for
		 * unmounting, This isn't a problem for rmdir because
		 * directories can't be unlinked unless they are empty.
		 */
		struct dir *dir = inode_dir_peek(inode);

		size_t size = dir->entries_size;
		struct direntry *entries = dir->entries;

		for (size_t ii = 0U; ii < size; ++ii) {
			struct direntry *entry = &entries[ii];

			free(entry->name);

			struct inode *inode = entry->inode;

			unsigned long handles_left;
			nlink_t links_left;
			{
				struct chanstat *stat = inode_stat_get_lock(inode);

				{
					unsigned long *refcount = inode_refcount_get_lock(inode);
					handles_left = *refcount;
					inode_refcount_put_lock(inode);
				}

				links_left = --stat->links;

				inode_stat_put_lock(inode);
			}

			if (0U == handles_left && 0U == links_left)
				inode_destroy(inode);
		}
		break;
	}
	}

free_node:
	;
	struct filesystem *fs = inode->impl.active.common.fs;

	inode->impl.common.active = false;

	pthread_spin_lock(&fs->lock);
	inode->impl.free.next = fs->free_inode;
	fs->free_inode = inode;
	++fs->stat.inodes;
	pthread_spin_unlock(&fs->lock);
}

static ino_t inode_number(struct inode *inode)
{
	return inode - inode->impl.active.common.fs->inode_table;
}

static enum inode_type inode_type(struct inode *inode)
{
	return inode->impl.active.common.type;
}

static struct inode *inode_get(struct inode *inode)
{
	struct inode *result;

	unsigned long *refcount = inode_refcount_get_lock(inode);

	unsigned long count = *refcount;
	if (((unsigned long)-1) == count) {
		result = NULL;
	} else {
		*refcount = count + 1U;

		result = inode;
	}

	inode_refcount_put_lock(inode);

	return result;
}

static void inode_put(struct inode *inode)
{
	unsigned long handles_left;
	nlink_t links_left;
	{
		struct chanstat *stat = inode_stat_get_lock(inode);

		{
			unsigned long *refcount = inode_refcount_get_lock(inode);

			handles_left = --*refcount;

			inode_refcount_put_lock(inode);
		}

		links_left = stat->links;

		inode_stat_put_lock(inode);
	}

	if (0U == handles_left && 0U == links_left)
		inode_destroy(inode);
}

static unsigned long *inode_refcount_get_lock(struct inode *inode)
{
	pthread_spin_lock(&inode->impl.active.common.refcount_lock);
	return &inode->impl.active.common.refcount;
}

static void inode_refcount_put_lock(struct inode *inode)
{
	pthread_spin_unlock(&inode->impl.active.common.refcount_lock);
}

static struct chanstat *inode_stat_get_lock(struct inode *file)
{
	pthread_spin_lock(&file->impl.active.common.stat_lock);
	return &file->impl.active.common.stat;
}

static void inode_stat_put_lock(struct inode *file)
{
	pthread_spin_unlock(&file->impl.active.common.stat_lock);
}

static struct dir *inode_dir_peek(struct inode *file)
{
	if (inode_type(file) != INODE_TYPE_DIR)
		return NULL;

	return &file->impl.active.dir;
}

static struct chan *inode_chan_peek(struct inode *file)
{
	if (inode_type(file) != INODE_TYPE_CHAN)
		return NULL;

	return &file->impl.active.chan;
}

static int inode_touch(struct inode *inode, int fields)
{
	int errnum = 0;

	if (fields & ~INODE_TOUCH_ALL)
		return -EINVAL;

	bool touch_access = (fields & INODE_TOUCH_ACCESS) != 0;
	bool touch_modify = (fields & INODE_TOUCH_MODIFY) != 0;
	bool touch_status = (fields & INODE_TOUCH_STATUS) != 0;

	struct chanstat *stat = inode_stat_get_lock(inode);

	/* Get the time after the lock because the lock can take some time */
	time_t sec;
	long nsec;
	{
		struct timespec xx;
		if (-1 == clock_gettime(CLOCK_REALTIME, &xx)) {
			errnum = -errno;
			goto unlock_stat;
		}
		sec = xx.tv_sec;
		nsec = xx.tv_nsec;
	}

	if (touch_access) {
		stat->last_access_time.tv_sec = sec;
		stat->last_access_time.tv_nsec = nsec;
	}

	if (touch_modify) {
		stat->last_modification_time.tv_sec = sec;
		stat->last_modification_time.tv_nsec = nsec;
	}

	if (touch_status) {
		stat->last_status_change.tv_sec = sec;
		stat->last_status_change.tv_nsec = nsec;
	}

unlock_stat:
	inode_stat_put_lock(inode);

	return errnum;
}


static int inode_utimens(struct inode *inode, struct timespec const times[2U])
{
	int errnum = 0;

	bool set_access_to_current = false;
	bool set_modify_to_current = false;

	bool dont_set_access = false;
	bool dont_set_modify = false;

	struct timespec last_access_time;
	struct timespec last_modify_time;

	if (NULL == times) {
		set_access_to_current = true;
		set_modify_to_current = true;
	} else {
		last_access_time = times[0U];
		last_modify_time = times[1U];

		switch (last_access_time.tv_nsec) {
		case UTIME_NOW:
			set_access_to_current = true;
			break;

		case UTIME_OMIT:
			dont_set_access = true;
			break;
		}

		switch (last_modify_time.tv_nsec) {
		case UTIME_NOW:
			set_modify_to_current = true;
			break;

		case UTIME_OMIT:
			dont_set_modify = true;
			break;
		}
	}

	struct chanstat *stat = inode_stat_get_lock(inode);

	uid_t owner = stat->owner;

	if ((!dont_set_access || !dont_set_modify) &&
	    owner != fuse_get_context()->uid) {
		errnum = -EPERM;
		goto unlock_stat;
	}

	/* Get the time after the lock because the lock can take some time */

	time_t sec;
	long nsec;
	if (set_access_to_current || set_modify_to_current) {
		struct timespec xx;
		if (-1 == clock_gettime(CLOCK_REALTIME, &xx)) {
			errnum = -errno;
			goto unlock_stat;
		}
		sec = xx.tv_sec;
		nsec = xx.tv_nsec;
	}

	if (set_modify_to_current) {
		stat->last_access_time.tv_sec = sec;
		stat->last_access_time.tv_nsec = nsec;
	} else if (!dont_set_access) {
		stat->last_access_time = last_access_time;
	}

	if (set_modify_to_current) {
		stat->last_modification_time.tv_sec = sec;
		stat->last_modification_time.tv_nsec = nsec;
	} else if (!dont_set_modify) {
		stat->last_modification_time = last_modify_time;
	}

unlock_stat:
	inode_stat_put_lock(inode);

	return errnum;
}

static int inode_access(struct inode *inode, int req)
{
	if ((req & ~(R_OK | W_OK | X_OK)) != 0)
		return -EINVAL;

	bool w_ok = (req & W_OK) != 0;
	bool r_ok = (req & R_OK) != 0;
	bool x_ok = (req & X_OK) != 0;

	mode_t mode;
	uid_t owner;
	gid_t group;
	{
		struct chanstat *stat = inode_stat_get_lock(inode);
		mode = stat->mode;
		owner = stat->owner;
		group = stat->group;
		inode_stat_put_lock(inode);
	}

	uid_t uid;
	gid_t gid;
	{
		struct fuse_context *context = fuse_get_context();
		uid = context->uid;
		gid = context->gid;
	}

	if (r_ok) {
		bool is_user_readable = (mode & S_IRUSR) != 0;
		bool is_group_readable = (mode & S_IRGRP) != 0;
		bool is_other_readable = (mode & S_IRGRP) != 0;

		bool allowed = false;

		if (is_user_readable && uid == owner)
			allowed = true;

		if (is_group_readable && gid == group)
			allowed = true;

		if (is_other_readable)
			allowed = true;

		if (!allowed)
			return -EPERM;
	}

	if (w_ok) {
		bool is_user_writeable = (mode & S_IWUSR) != 0;
		bool is_group_writeable = (mode & S_IWGRP) != 0;
		bool is_other_writeable = (mode & S_IWGRP) != 0;

		bool allowed = false;

		if (is_user_writeable && uid == owner)
			allowed = true;

		if (is_group_writeable && gid == group)
			allowed = true;

		if (is_other_writeable)
			allowed = true;

		if (!allowed)
			return -EPERM;
	}

	if (x_ok) {
		bool is_user_executable = (mode & S_IXUSR) != 0;
		bool is_group_executable = (mode & S_IXGRP) != 0;
		bool is_other_executable = (mode & S_IXGRP) != 0;

		bool allowed = false;

		if (is_user_executable && uid == owner)
			allowed = true;

		if (is_group_executable && gid == group)
			allowed = true;

		if (is_other_executable)
			allowed = true;

		if (!allowed)
			return -EPERM;
	}

	return 0;
}

static int inode_readdir(struct inode *inode, void *buf, fuse_fill_dir_t filler)
{
	struct dir *dir = inode_dir_peek(inode);
	if (NULL == dir)
		return -ENOTDIR;

	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);

	pthread_spin_lock(&dir->lock);

	size_t size = dir->entries_size;
	struct direntry * entries = dir->entries;

	for (size_t ii = 0U; ii < size; ++ii)
		filler(buf, entries[ii].name, NULL, 0);

	pthread_spin_unlock(&dir->lock);

	return inode_touch(inode, INODE_TOUCH_ACCESS);
}

static int inode_poll(struct inode *inode,
		      struct fuse_pollhandle *ph, unsigned *reventsp,
		      struct poller **pollerp)
{
	bool has_reader;

	struct chan *chan = inode_chan_peek(inode);
	if (NULL == chan)
		return -EISDIR;

	struct poller *poller = NULL;

	if (ph != NULL) {
		poller = malloc(sizeof *poller);
		if (NULL == poller)
			return -errno;

		poller->poll_handle = ph;
		poller->next = NULL;
	}

	pthread_mutex_lock(&chan->lock);

	has_reader = chan->pending_reader != NULL;

	if (poller != NULL) {
		poller->next = chan->first_poller;
		chan->first_poller = poller;
	}

	pthread_mutex_unlock(&chan->lock);

	unsigned revents = 0U;
	if (has_reader)
		revents |= POLLOUT;
	else
		revents |= POLLIN;

	*reventsp = revents;

	*pollerp = poller;

	return 0;
}

static int inode_destroy_poller(struct inode *inode, struct poller *poller)
{
	struct chan *chan = inode_chan_peek(inode);

	pthread_mutex_lock(&chan->lock);

	struct poller **pollerp = &chan->first_poller;
	for (;;pollerp = &(*pollerp)->next) {
		if (*pollerp == poller) {
			*pollerp = poller->next;
			break;
		}
	}

	pthread_mutex_unlock(&chan->lock);

	fuse_pollhandle_destroy(poller->poll_handle);
	free(poller);

	return 0;
}

static int inode_read(struct inode *inode, struct fuse_bufvec **bufp,
		      size_t size,
		      bool nonblocking)
{
	int errnum = 0;

	struct chan *chan = inode_chan_peek(inode);
	if (NULL == chan)
		return -EISDIR;

	pthread_mutex_lock(&chan->lock);

	if (NULL == chan->pending_reader)
		goto wait_for_writer;

	if (nonblocking) {
		errnum = -EAGAIN;
		goto unlock_lock;
	}

	for (;;) {
		if (NULL == chan->pending_reader)
			break;

		pthread_cond_wait(&chan->cond, &chan->lock);

		if (fuse_interrupted()) {
			errnum = -EINTR;
			goto unlock_lock;
		}
	}

wait_for_writer:
	;
	struct fuse_bufvec *buf = NULL;
	chan->pending_reader = &buf;
	chan->pending_read_size = size;

	pthread_cond_broadcast(&chan->cond);
	for (struct poller *poller = chan->first_poller;
	     poller != NULL; poller = poller->next)
		fuse_notify_poll(poller->poll_handle);

	for (;;) {
		pthread_cond_wait(&chan->cond, &chan->lock);

		if (buf != NULL)
			break;

		if (fuse_interrupted()) {
			errnum = -EINTR;
			chan->pending_reader = NULL;
			goto unlock_lock;
		}
	}

unlock_lock:
	pthread_mutex_unlock(&chan->lock);
	if (errnum < 0)
		return errnum;

	*bufp = buf;

	errnum = inode_touch(inode, INODE_TOUCH_ACCESS);
	if (errnum < 0)
		return errnum;

	return fuse_buf_size(buf);
}

static int inode_write(struct inode *file, struct fuse_bufvec *buf_orig,
		       bool nonblocking)
{
	int errnum = 0;

	struct chan *chan = inode_chan_peek(file);
	if (NULL == chan)
		return -EISDIR;

	size_t size = fuse_buf_size(buf_orig);

	char *memory = malloc(size);
	if (NULL == memory) {
		return -errno;
	}

	struct fuse_bufvec *buf = malloc(sizeof *buf);
	if (NULL == buf) {
		free(buf);
		return -errno;
	}

	buf->count = 1U;
	buf->idx = 0U;
	buf->off = 0U;
	buf->buf[0U].size = size;
	buf->buf[0U].flags = 0;
	buf->buf[0U].mem = memory;

	pthread_mutex_lock(&chan->lock);

	struct fuse_bufvec **pending_reader = chan->pending_reader;
	if (pending_reader != NULL)
		goto write_buffer;

	if (nonblocking) {
		errnum = -EAGAIN;
		goto unlock_lock;
	}

	for (;;) {
		pending_reader = chan->pending_reader;
		if (pending_reader != NULL)
			break;

		pthread_cond_wait(&chan->cond, &chan->lock);

		if (fuse_interrupted()) {
			errnum = -EINTR;
			goto unlock_lock;
		}
	}

write_buffer:
	if (chan->pending_read_size != size) {
		errnum = -EPROTO;
		goto unlock_lock;
	}

	ssize_t copy = fuse_buf_copy(buf, buf_orig, 0);
	if (copy < 0) {
		errnum = copy;
		goto unlock_lock;
	}

	*pending_reader = buf;
	chan->pending_reader = NULL;
	pthread_cond_broadcast(&chan->cond);
	for (struct poller *poller = chan->first_poller;
	     poller != NULL; poller = poller->next)
		fuse_notify_poll(poller->poll_handle);

unlock_lock:
	pthread_mutex_unlock(&chan->lock);

	if (errnum < 0) {
		free(memory);
		free(buf);
		return errnum;
	}

	if (size > 0U) {
		errnum = inode_touch(file, INODE_TOUCH_MODIFY | INODE_TOUCH_STATUS);
		if (errnum < 0)
			return errnum;
	}

	return size;
}

static int inode_chmod(struct inode *inode, mode_t mode)
{
	int errnum = 0;

	/* Strangely, FUSE is buggy and adds in a sign bit */
	mode_t sign_bit = 0x8000;

	mode_t setxid_bits = S_ISUID | S_ISGID;
	mode_t sticky_bit = S_ISVTX;
	mode_t user_bits = S_IRUSR | S_IWUSR | S_IXUSR;
	mode_t group_bits = S_IRGRP | S_IWGRP | S_IXGRP;
	mode_t other_bits = S_IROTH | S_IWOTH | S_IXOTH;

	mode_t valid_mode_flags = sign_bit |
		setxid_bits |
		sticky_bit |
		user_bits |
		group_bits |
		other_bits;
	if ((mode & ~valid_mode_flags) != 0)
		return -EINVAL;

	uid_t uid;
	{
		struct fuse_context *context = fuse_get_context();
		uid = context->uid;
	}

	{
		struct chanstat *stat = inode_stat_get_lock(inode);
		if (uid != stat->owner){
			errnum = -EPERM;
			goto unlock_stat;
		}

		stat->mode = mode;

	unlock_stat:
		inode_stat_put_lock(inode);
	}
	if (errnum < 0)
		goto finish;

	errnum = inode_touch(inode, INODE_TOUCH_STATUS);

finish:
	return errnum;
}

static int inode_chown(struct inode *inode, uid_t owner,
		       gid_t group)
{
	int errnum = 0;

	bool change_owner = owner != (uid_t)-1;
	bool change_group = group != (gid_t)-1;

	/* TODO: Use capabilities to let people change the owner of a file */
	if (change_owner)
		return -EPERM;

	if (!change_group)
		return 0;

	uid_t uid;
	gid_t gid;
	{
		struct fuse_context *context = fuse_get_context();
		uid = context->uid;
		gid = context->gid;
	}

	/*
	 * The owner of a file may change the group of the file to any
	 * group of which that owner is a member. Unfortunately, FUSE
	 * does not expose to us the contents of supplementary group
	 * IDs so we cannot check for them.
	 */
	int groups_count = fuse_getgroups(-1, NULL);
	if (-1 == groups_count) {
		errnum = -errno;
		/* This feature is unsupported */
		if (-ENOSYS == errnum) {
			errnum = 0;
			goto check_gid;
		}
		return errnum;
	}

	gid_t *gids = calloc(groups_count, sizeof gids[0U]);
	if (NULL == gids)
		return -errno;

	if (-1 == fuse_getgroups(groups_count, gids)) {
		errnum = -errno;
		goto free_gids;
	}

	for (size_t ii = 0U; ii < groups_count; ++ii) {
		if (group == gids[ii])
			goto is_in_group;
	}

free_gids:
	free(gids);

check_gid:
	if (group != gid)
		return -EPERM;

is_in_group:
	{
		struct chanstat *stat = inode_stat_get_lock(inode);
		if (uid != stat->owner){
			errnum = -EPERM;
			goto unlock_stat;
		}

		stat->group = group;
		stat->mode = ~(S_ISUID | S_ISGID) & stat->mode;

	unlock_stat:
		inode_stat_put_lock(inode);
	}
	if (errnum < 0)
		goto finish;

	errnum = inode_touch(inode, INODE_TOUCH_STATUS);

finish:
	return errnum;
}

static int inode_open(struct inode **inodep,
		      struct inode *dirinode, char const *path,
		      mode_t mode, unsigned flags)
{
	int errnum = 0;

	bool o_creat = (flags & O_CREAT) != 0;
	bool o_excl = (flags & O_EXCL) != 0;

	bool o_rdonly = (flags & O_RDONLY) != 0;
	bool o_wronly = (flags & O_WRONLY) != 0;
	bool o_rdwr = (flags & O_RDWR) != 0;

	bool o_append = (flags & O_APPEND) != 0;

	bool o_directory = (flags & O_DIRECTORY) != 0;

	if (o_excl && !o_creat)
		return -EINVAL;

	if (o_directory && o_creat)
		return -EINVAL;

	/* Some applications open directories with the following even
	 * though they don't make sense for directories. Be
	 * compatible.
	 */
	if (o_directory && o_append)
		return -EINVAL;

	int perms = 0;
	if (o_rdonly || o_rdwr)
		perms |= R_OK;
	if (o_wronly || o_rdwr)
		perms |= W_OK;

	struct inode *inode = NULL;
	if (0 == strcmp("", path)) {
		if (o_excl) {
			errnum = -EEXIST;
			goto put_dirinode;
		}

		errnum = inode_access(dirinode, perms);
		if (errnum < 0)
			goto put_dirinode;

		inode = inode_get(dirinode);
	} else {
		errnum = inode_access(dirinode, X_OK);
		if (errnum < 0)
			goto put_dirinode;

		struct dir *dir = inode_dir_peek(dirinode);
		if (NULL == dir) {
			errnum = -ENOTDIR;
			goto put_dirinode;
		}

		pthread_spin_lock(&dir->lock);

		size_t size = dir->entries_size;
		struct direntry * entries = dir->entries;

		for (size_t ii = 0U; ii < size; ++ii) {
			struct direntry * entry = &entries[ii];

			if (strcmp(path, entry->name) != 0)
				continue;

			if (o_excl) {
				errnum = -EEXIST;
				break;
			}

			inode = inode_get(entry->inode);
		}
		if (inode != NULL) {
			if (o_directory && inode_type(inode) != INODE_TYPE_DIR) {
				errnum = -ENOTDIR;
				goto unlock_lock;
			}

			errnum = inode_access(inode, perms);
			goto unlock_lock;
		}

		if (!o_creat || o_directory) {
			errnum = -ENOENT;
			goto unlock_lock;
		}

		char *path_dup = strdup(path);
		if (NULL == path_dup) {
			errnum = -errno;
			goto unlock_lock;
		}

		struct fuse_context *context = fuse_get_context();
		uid_t uid = context->uid;
		gid_t gid = context->gid;

		struct filesystem *fs = dirinode->impl.active.common.fs;
		struct inode *new_inode;
		{
			struct inode *xx;
			errnum = filesystem_inode_create(fs, &xx, INODE_TYPE_CHAN,
							 mode, uid, gid);
			if (errnum < 0)
				goto free_path;
			new_inode = xx;
		}

		size_t new_size = size + 1U;
		struct direntry *new_entries = realloc(entries,
						       sizeof entries[0U] * new_size);
		if (NULL == new_entries) {
			errnum = -errno;
			goto put_inode;
		}

		new_entries[size].name = path_dup;
		new_entries[size].inode = new_inode;
		dir->entries = new_entries;
		dir->entries_size = new_size;

		++new_inode->impl.active.common.stat.links;

		inode = new_inode;

		inode_touch(dirinode,
			    INODE_TOUCH_MODIFY | INODE_TOUCH_STATUS);

	put_inode:
		if (errnum < 0)
			inode_put(new_inode);

	free_path:
		if (errnum < 0)
			free(path_dup);

	unlock_lock:
		pthread_spin_unlock(&dir->lock);
	}
put_dirinode:
	if (errnum < 0)
		goto put_file;

	*inodep = inode;

put_file:
	if (errnum < 0 && inode != NULL)
		inode_put(inode);

	return errnum;
}

static int inode_truncate(struct inode *inode, off_t size)
{
	struct chan * chan = inode_chan_peek(inode);
	if (NULL == chan)
		return -EISDIR;

	return 0;
}

static int inode_rename(struct inode *inode, char const * oldpath,
			char const *newpath)
{
	struct dir *dir = inode_dir_peek(inode);
	if (NULL == dir)
		return -ENOTDIR;

	return -EPERM;
}

static int inode_unlink(struct inode *inode, char const *path)
{
	struct dir *dir = inode_dir_peek(inode);
	if (NULL == dir)
		return -ENOTDIR;

	return -EPERM;
}

static struct file_desc *peek_file_info_desc(struct fuse_file_info *info)
{
	return (struct file_desc*)info->fh;
}

static void set_file_info_desc(struct fuse_file_info *info,
			       struct file_desc *desc)
{
	info->direct_io = true;
	info->keep_cache = false;
	info->nonseekable = true;
	info->fh = (unsigned long)desc;
}

static void secure_zero(void volatile *buf, size_t size)
{
	memset((void *)buf, 0, size);

	for (size_t ii = 0U; ii < size; ++ii)
		__asm__ volatile ("" : "=m"((((char *)buf)[ii])));
}
