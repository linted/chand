all: chand

chand: main.c
	clang -fsanitize=address -fsanitize=undefined -g -o $@ $^ -std=c99 -D_FILE_OFFSET_BITS=64 $$(pkg-config --cflags --libs fuse)
